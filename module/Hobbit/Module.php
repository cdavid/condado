<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/Hobbit for the canonical source repository
 * @copyright Copyright (c) 2005-2012 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Hobbit;

use Zend\ModuleManager\Feature\AutoloaderProviderInterface;
use Zend\Mvc\ModuleRouteListener;
use Zend\Mvc\MvcEvent;
use Hobbit\Model\HobbitTable;
use Zend\Db\TableGateway\TableGateway;
use Zend\ServiceManager\ServiceManager;
use Zend\Db\ResultSet\ResultSet;
use Hobbit\Model\Hobbit;
use Hobbit\Model\Tocas;
use Hobbit\Model\TocasTable;

class Module implements AutoloaderProviderInterface
{
    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\ClassMapAutoloader' => array(
                __DIR__ . '/autoload_classmap.php',
            ),
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
		    // if we're in a namespace deeper than one level we need to fix the \ in the path
                    __NAMESPACE__ => __DIR__ . '/src/' . str_replace('\\', '/' , __NAMESPACE__),
                ),
            ),
        );
    }

    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    public function onBootstrap(MvcEvent $e)
    {
        // You may not need to do this if you're doing it elsewhere in your
        // application
        $eventManager        = $e->getApplication()->getEventManager();
        $moduleRouteListener = new ModuleRouteListener();
        $moduleRouteListener->attach($eventManager);
    }
    
    public function getServiceConfig()
    {
    	return array(
    		'factories' => array(
    		  'HobbitTable' => function(ServiceManager $sm){
                  $adapter      = $sm->get('Zend\Db\Adapter');
                  $resultSetPrototype = new ResultSet();
                  $resultSetPrototype->setArrayObjectPrototype(new Hobbit($sm));
    		      $tableGateway = new TableGateway('hobbits', $adapter, null, $resultSetPrototype);
    		      $hobbitTable  = new HobbitTable($tableGateway);
    		      return $hobbitTable;
    		  },
    		  'TocasTable' => function(ServiceManager $sm){
    		      $adapter      = $sm->get('Zend\Db\Adapter');
    		      $resultSetPrototype = new ResultSet();
    		      $resultSetPrototype->setArrayObjectPrototype(new Tocas());
    		      $tableGateway = new TableGateway('tocas', $adapter, null, $resultSetPrototype);
    		      $tocasTable  = new TocasTable($tableGateway);
    		      return $tocasTable;
    		  }
    	    ) 
    	);
    }
}
