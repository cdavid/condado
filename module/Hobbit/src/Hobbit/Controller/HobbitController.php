<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/Hobbit for the canonical source repository
 * @copyright Copyright (c) 2005-2012 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Hobbit\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Hobbit\Form\Hobbit as HobbitForm;
use Hobbit\Model\Hobbit;
use Hobbit\Model\HobbitTable;

class HobbitController extends AbstractActionController
{
    public function indexAction()
    {
        $hobbitTable = $this->getHobbitTable();
        $records = $hobbitTable->fecthAll();
        return array('records' => $records);
    }

    public function editAction()
    {
        $id = $this->params('id',null);
        
        if(isset($_SESSION['form']))
        {
        	$form = $_SESSION['form'];
        	unset($_SESSION['form']);
        } else {
            $form = new HobbitForm();
        }
        
        $form->setAttribute('action', $this->
            url()->fromRoute('hobbit',array(
                'action' => 'save'
        )));
        $form->setId($id, $this->getServiceLocator());
        
        return array('form' => $form);
    }
    
    public function saveAction()
    {
        // abaixo cria o objeto model
    	$hobbit = new Hobbit($this->getServiceLocator());
    	$hobbit->exchangeArray((array)$this->getRequest()->getPost()); // o array colocado � pra for�ar o tipo � o que chamamos de cast
    	// abaixo cria o form e pega o inputFilter do obrjeto model
    	$form = new HobbitForm();
    	$form->setId($hobbit->id, $this->getServiceLocator());
    	$form->setInputFilter($hobbit->getInputFilter());
    	$form->bind($hobbit); // recebe os dados
    	if(!$form->isValid())
    	{
    		$_SESSION['form'] = $form;
    		return $this->redirect()->toRoute('hobbit',array('action' => 'edit'));
    	}
    	
    	$hobbitTable = $this->getHobbitTable();
    	$hobbitTable->save($hobbit);
    	
    	$this->redirect()->toRoute('hobbit');
    }
    
    public function deleteAction()
    {
        $id = $this->params('id',null);
        $hobbitTable = $this->getHobbitTable();
        $hobbitTable->delete($id);
        
        $this->redirect()->toRoute('hobbit');
    }
    
    // com a sinalizacao abaixo faz com que o ao chamar o metodo traga os metodos que o service locator retorna
    /**
     * 
     * @return HobbitTable
     */
    private function getHobbitTable()
    {
    	return $this->getServiceLocator()->get('HobbitTable');
    }
    
}
