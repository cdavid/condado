<?php
namespace Hobbit\Model;

use Zend\Db\TableGateway\TableGateway;
class HobbitTable
{
    /**
     * 
     * @var TableGateway
     */
    private $tableGateway = null;
    
    public function __construct(TableGateway $tableGateway)
    {
    	$this->tableGateway = $tableGateway;
    }
    
    public function save(Hobbit $hobbit)
    {
        $set = array(
        	'nome'     => $hobbit->nome,
            'id_tocas' => $hobbit->tocas->id
        );
        
        if($hobbit->id == null)
        {
            $this->tableGateway->insert($set); // possui retorno 0:Falso e 1:Verdadeiro
        }
        else {
            $where = array('id' => $hobbit->id);
        	$this->tableGateway->update($set,$where);
        }
    	
    }
    
    public function fecthAll($where = null)
    {
        return $this->tableGateway->select($where);
    }
    
    public function fetchOne($id)
    {
    	$where = array('id' => $id);
    	$resultSet = $this->fecthAll($where);
    	if($resultSet->count() == 1)
    	{
    		return $resultSet->current();
    	}
    	return null;
    }
    
    public function delete($id)
    {
        $where = array('id' => $id);
    	$this->tableGateway->delete($where);
    }
}

?>