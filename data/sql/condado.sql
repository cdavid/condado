-- phpMyAdmin SQL Dump
-- version 3.5.6
-- http://www.phpmyadmin.net
--
-- Máquina: 127.0.0.1:3306
-- Data de Criação: 27-Jul-2014 às 21:40
-- Versão do servidor: 5.5.23
-- versão do PHP: 5.4.16

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de Dados: `condado`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `hobbits`
--

CREATE TABLE IF NOT EXISTS `hobbits` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `id_tocas` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=10 ;

--
-- Extraindo dados da tabela `hobbits`
--

INSERT INTO `hobbits` (`id`, `nome`, `id_tocas`) VALUES
(1, 'Cebolinha', 3),
(2, 'Monicare', 4),
(3, 'Cascão', 6),
(5, 'Xaveco', 4),
(6, 'Thunder', 3),
(9, 'Fradore', 3);

-- --------------------------------------------------------

--
-- Estrutura da tabela `papeis`
--

CREATE TABLE IF NOT EXISTS `papeis` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `papel` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Extraindo dados da tabela `papeis`
--

INSERT INTO `papeis` (`id`, `papel`) VALUES
(1, 'basico');

-- --------------------------------------------------------

--
-- Estrutura da tabela `papeis_usuario`
--

CREATE TABLE IF NOT EXISTS `papeis_usuario` (
  `id_papel` int(11) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  PRIMARY KEY (`id_papel`,`id_usuario`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `papeis_usuario`
--

INSERT INTO `papeis_usuario` (`id_papel`, `id_usuario`) VALUES
(1, 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `recursos`
--

CREATE TABLE IF NOT EXISTS `recursos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `recurso` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

--
-- Extraindo dados da tabela `recursos`
--

INSERT INTO `recursos` (`id`, `recurso`) VALUES
(1, '/hobbit'),
(2, '/tocas'),
(3, '/hobbit/edit'),
(4, '/tocas/edit');

-- --------------------------------------------------------

--
-- Estrutura da tabela `recursos_papel`
--

CREATE TABLE IF NOT EXISTS `recursos_papel` (
  `id_recurso` int(11) NOT NULL,
  `id_papel` int(11) NOT NULL,
  PRIMARY KEY (`id_recurso`,`id_papel`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tocas`
--

CREATE TABLE IF NOT EXISTS `tocas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=8 ;

--
-- Extraindo dados da tabela `tocas`
--

INSERT INTO `tocas` (`id`, `nome`) VALUES
(3, 'TocadoBilbore'),
(4, 'Toca do Sam'),
(6, 'TocaDoElfo'),
(7, 'TocaDoAnão');

-- --------------------------------------------------------

--
-- Estrutura da tabela `usuarios`
--

CREATE TABLE IF NOT EXISTS `usuarios` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `usuario` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `senha` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Extraindo dados da tabela `usuarios`
--

INSERT INTO `usuarios` (`id`, `usuario`, `senha`) VALUES
(1, 'cebolinha', '81dc9bdb52d04dc20036dbd8313ed055');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
